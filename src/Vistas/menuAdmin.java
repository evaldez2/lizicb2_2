package Vistas;

import Controladores.menuadmincontrolador;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class menuAdmin extends javax.swing.JFrame {
    menuadmincontrolador mac;
    
    public menuAdmin() {
        initComponents();
        controlador();
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        this.ventasButton.setCursor(new Cursor(HAND_CURSOR));
        this.clientesButton.setCursor(new Cursor(HAND_CURSOR));
        this.usuariosButton.setCursor(new Cursor(HAND_CURSOR));
        this.inventarioButton.setCursor(new Cursor(HAND_CURSOR));
        this.cerrarsesionButton.setCursor(new Cursor(HAND_CURSOR));
        this.reporteButton.setCursor(new Cursor(HAND_CURSOR));
        setTitle("      Menu-Admin");
        setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
        this.setLocationRelativeTo(this);
    }

    public void controlador(){
        mac = new menuadmincontrolador(this);
        ventasButton.addActionListener(mac);
        clientesButton.addActionListener(mac);
        usuariosButton.addActionListener(mac);
        inventarioButton.addActionListener(mac);
        cerrarsesionButton.addActionListener(mac);
        reporteButton.addActionListener(mac);
        this.HerramientaButton.addActionListener(mac);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cerrarsesionButton = new javax.swing.JButton();
        inventarioButton = new javax.swing.JButton();
        usuariosButton = new javax.swing.JButton();
        clientesButton = new javax.swing.JButton();
        ventasButton = new javax.swing.JButton();
        reporteButton = new javax.swing.JButton();
        HerramientaButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        cerrarsesionButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btncerrarsesion.png"))); // NOI18N
        cerrarsesionButton.setActionCommand("cerrarsesion");
        cerrarsesionButton.setBorderPainted(false);
        cerrarsesionButton.setContentAreaFilled(false);
        cerrarsesionButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cerrarsesionButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cerrarsesionButtonMouseExited(evt);
            }
        });
        getContentPane().add(cerrarsesionButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 310, 140, 80));

        inventarioButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btninventario.png"))); // NOI18N
        inventarioButton.setActionCommand("inventario");
        inventarioButton.setBorderPainted(false);
        inventarioButton.setContentAreaFilled(false);
        inventarioButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                inventarioButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                inventarioButtonMouseExited(evt);
            }
        });
        getContentPane().add(inventarioButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 310, 130, 80));

        usuariosButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnusuarios.png"))); // NOI18N
        usuariosButton.setActionCommand("usuarios");
        usuariosButton.setBorderPainted(false);
        usuariosButton.setContentAreaFilled(false);
        usuariosButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                usuariosButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                usuariosButtonMouseExited(evt);
            }
        });
        getContentPane().add(usuariosButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 220, 140, 80));

        clientesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnclientes.png"))); // NOI18N
        clientesButton.setActionCommand("clientes");
        clientesButton.setBorderPainted(false);
        clientesButton.setContentAreaFilled(false);
        clientesButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                clientesButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                clientesButtonMouseExited(evt);
            }
        });
        getContentPane().add(clientesButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 220, 140, 80));

        ventasButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnventas.png"))); // NOI18N
        ventasButton.setActionCommand("ventas");
        ventasButton.setBorderPainted(false);
        ventasButton.setContentAreaFilled(false);
        ventasButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                ventasButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ventasButtonMouseExited(evt);
            }
        });
        getContentPane().add(ventasButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 220, 140, 80));

        reporteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnreportes.png"))); // NOI18N
        reporteButton.setActionCommand("reportes");
        reporteButton.setBorderPainted(false);
        reporteButton.setContentAreaFilled(false);
        reporteButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reporteButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reporteButtonMouseExited(evt);
            }
        });
        getContentPane().add(reporteButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 310, 130, 80));

        HerramientaButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnherramientas.png"))); // NOI18N
        HerramientaButton.setActionCommand("Herramientas");
        HerramientaButton.setBorderPainted(false);
        HerramientaButton.setContentAreaFilled(false);
        HerramientaButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                HerramientaButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                HerramientaButtonMouseExited(evt);
            }
        });
        HerramientaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HerramientaButtonActionPerformed(evt);
            }
        });
        getContentPane().add(HerramientaButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 390, 130, 80));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/nuevos-productos.gif"))); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/productos.gif"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/fondo_menuAdmin.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ventasButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ventasButtonMouseEntered
         ventasButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnventasefecto.png")));
    }//GEN-LAST:event_ventasButtonMouseEntered

    private void ventasButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ventasButtonMouseExited
        ventasButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnventas.png")));
    }//GEN-LAST:event_ventasButtonMouseExited

    private void clientesButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_clientesButtonMouseEntered
        clientesButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnclientesefecto.png")));
    }//GEN-LAST:event_clientesButtonMouseEntered

    private void clientesButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_clientesButtonMouseExited
         clientesButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnclientes.png")));
    }//GEN-LAST:event_clientesButtonMouseExited

    private void usuariosButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_usuariosButtonMouseEntered
         usuariosButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnusuariosefecto.png")));
    }//GEN-LAST:event_usuariosButtonMouseEntered

    private void usuariosButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_usuariosButtonMouseExited
         usuariosButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnusuarios.png")));
    }//GEN-LAST:event_usuariosButtonMouseExited

    private void inventarioButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_inventarioButtonMouseEntered
         inventarioButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btninventarioefecto.png")));
    }//GEN-LAST:event_inventarioButtonMouseEntered

    private void inventarioButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_inventarioButtonMouseExited
        inventarioButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btninventario.png")));
    }//GEN-LAST:event_inventarioButtonMouseExited

    private void cerrarsesionButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cerrarsesionButtonMouseEntered
        cerrarsesionButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btncerrarsesionefecto.png")));
    }//GEN-LAST:event_cerrarsesionButtonMouseEntered

    private void cerrarsesionButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cerrarsesionButtonMouseExited
        cerrarsesionButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btncerrarsesion.png")));
    }//GEN-LAST:event_cerrarsesionButtonMouseExited

    private void reporteButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reporteButtonMouseEntered
       reporteButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnreportesefecto.png")));
    }//GEN-LAST:event_reporteButtonMouseEntered

    private void reporteButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reporteButtonMouseExited
        reporteButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnreportes.png")));
    }//GEN-LAST:event_reporteButtonMouseExited

    private void HerramientaButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_HerramientaButtonMouseEntered
        HerramientaButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnherramientasefecto.png")));
    }//GEN-LAST:event_HerramientaButtonMouseEntered

    private void HerramientaButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_HerramientaButtonMouseExited
        HerramientaButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnherramientas.png")));
    }//GEN-LAST:event_HerramientaButtonMouseExited

    private void HerramientaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HerramientaButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_HerramientaButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(menuAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(menuAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(menuAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(menuAdmin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new menuAdmin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton HerramientaButton;
    private javax.swing.JButton cerrarsesionButton;
    private javax.swing.JButton clientesButton;
    private javax.swing.JButton inventarioButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JButton reporteButton;
    private javax.swing.JButton usuariosButton;
    private javax.swing.JButton ventasButton;
    // End of variables declaration//GEN-END:variables
}
