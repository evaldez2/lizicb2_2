package Vistas;

import Controladores.inciarsesioncontrolador;
import Modelos.iniciarsesionmodelo;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.ImageIcon;

public class Inicio_sesion extends javax.swing.JFrame {

    //INSTANCIAMOS EL CONTROLADOR Y EL MODELO
    inciarsesioncontrolador isc;
    iniciarsesionmodelo ism;
    
    //CONSTRUCTOR DE LA VISTA INICIAR SESION
    public Inicio_sesion() {
        initComponents();
        controlador();
        this.setLocationRelativeTo(this); 
        this.setResizable(false); 
        this.setTitle("      Lizi Cosmetic"); 
        this.iniciarsesionButton.setCursor(new Cursor(HAND_CURSOR));
        this.setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage()); //ICONO DE LA VENTANA
    }

    
    private void controlador(){
        isc = new inciarsesioncontrolador(this);
        iniciarsesionButton.addActionListener(isc);
    }
    
   
    public iniciarsesionmodelo obtenerdatos(){       
        ism = new iniciarsesionmodelo();   
        ism.setUsuario(usuarioTextField.getText());      
        ism.setContraseña(String.valueOf(contraseñaTextField.getPassword())); //CONVERTIMOS TIPO PASSWORD A TIPO STRING MEDIANTE STRING.VALUEOF()
        return ism; //RETORNAMOS LOS DATOS A NUESTRO MODELO
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        iniciarsesionButton = new javax.swing.JButton();
        contraseñaTextField = new javax.swing.JPasswordField();
        usuarioTextField = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        iniciarsesionButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/entrar.png"))); // NOI18N
        iniciarsesionButton.setActionCommand("iniciarsesion");
        iniciarsesionButton.setBorderPainted(false);
        iniciarsesionButton.setContentAreaFilled(false);
        getContentPane().add(iniciarsesionButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 330, 250, 30));
        getContentPane().add(contraseñaTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 280, 200, 30));
        getContentPane().add(usuarioTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 230, 200, 30));

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/contraseña.png"))); // NOI18N
        jButton2.setBorderPainted(false);
        jButton2.setContentAreaFilled(false);
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 280, 40, 30));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/usuario.png"))); // NOI18N
        jButton1.setBorderPainted(false);
        jButton1.setContentAreaFilled(false);
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 230, 40, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/loginv2.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 411, 477));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField contraseñaTextField;
    private javax.swing.JButton iniciarsesionButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField usuarioTextField;
    // End of variables declaration//GEN-END:variables
}
