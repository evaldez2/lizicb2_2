package Vistas;

import Controladores.Registro_clientes_colaborador_controlador;
import Controladores.Registro_clientes_controlador;
import Controladores.Registro_usuarios_controlador;
import Modelos.ame_usuario_modelo;
import Modelos.cargartablas;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Registro_Clientes_Colaborador extends javax.swing.JFrame {

    cargartablas ut = new cargartablas();
    Registro_clientes_colaborador_controlador rcc;

    public Registro_Clientes_Colaborador() {
        initComponents();
        controlador();
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        ut.cargartablaclientes(usuariosTable);
        setTitle("      Registro de clientes");
        setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
        this.setLocationRelativeTo(this);
        this.regresarButton.setCursor(new Cursor(HAND_CURSOR));
        this.refrescartablaButton.setCursor(new Cursor(HAND_CURSOR));
        this.añadirButton.setCursor(new Cursor(HAND_CURSOR));
        this.modificarButton.setCursor(new Cursor(HAND_CURSOR));
    }

    public void controlador() {
        rcc = new Registro_clientes_colaborador_controlador(this);
        añadirButton.addActionListener(rcc);
        modificarButton.addActionListener(rcc);
        refrescartablaButton.addActionListener(rcc);
        regresarButton.addActionListener(rcc);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        usuariosTable = new javax.swing.JTable();
        añadirButton = new javax.swing.JButton();
        modificarButton = new javax.swing.JButton();
        refrescartablaButton = new javax.swing.JButton();
        regresarButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        usuariosTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID_Cliente", "Nombre", "Apellido", "Telefono"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(usuariosTable);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 230, 800, 270));

        añadirButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadir.png"))); // NOI18N
        añadirButton.setActionCommand("Añadir");
        añadirButton.setBorderPainted(false);
        añadirButton.setContentAreaFilled(false);
        añadirButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                añadirButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                añadirButtonMouseExited(evt);
            }
        });
        getContentPane().add(añadirButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 150, 130, 70));

        modificarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1.png"))); // NOI18N
        modificarButton.setActionCommand("Modificar");
        modificarButton.setBorderPainted(false);
        modificarButton.setContentAreaFilled(false);
        modificarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                modificarButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                modificarButtonMouseExited(evt);
            }
        });
        getContentPane().add(modificarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 150, 130, 70));

        refrescartablaButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnactualizar.png"))); // NOI18N
        refrescartablaButton.setActionCommand("RefrescarTabla");
        refrescartablaButton.setBorderPainted(false);
        refrescartablaButton.setContentAreaFilled(false);
        refrescartablaButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                refrescartablaButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                refrescartablaButtonMouseExited(evt);
            }
        });
        getContentPane().add(refrescartablaButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 150, 130, 70));

        regresarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnatras.png"))); // NOI18N
        regresarButton.setActionCommand("Regresar Al Menu");
        regresarButton.setBorderPainted(false);
        regresarButton.setContentAreaFilled(false);
        getContentPane().add(regresarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 10, 40, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/fondo_clientes.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void refrescartablaButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_refrescartablaButtonMouseEntered
        refrescartablaButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnactualizarefecto.png")));
    }//GEN-LAST:event_refrescartablaButtonMouseEntered

    private void refrescartablaButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_refrescartablaButtonMouseExited
        refrescartablaButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnactualizar.png")));
    }//GEN-LAST:event_refrescartablaButtonMouseExited

    private void añadirButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_añadirButtonMouseEntered
        añadirButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadirefecto.png")));
    }//GEN-LAST:event_añadirButtonMouseEntered

    private void añadirButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_añadirButtonMouseExited
        añadirButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadir.png")));
    }//GEN-LAST:event_añadirButtonMouseExited

    private void modificarButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modificarButtonMouseEntered
        modificarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1efecto.png")));
    }//GEN-LAST:event_modificarButtonMouseEntered

    private void modificarButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modificarButtonMouseExited
        modificarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1.png")));
    }//GEN-LAST:event_modificarButtonMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Registro_Clientes_Colaborador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Registro_Clientes_Colaborador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Registro_Clientes_Colaborador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Registro_Clientes_Colaborador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Registro_Clientes_Colaborador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton añadirButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton modificarButton;
    private javax.swing.JButton refrescartablaButton;
    private javax.swing.JButton regresarButton;
    public javax.swing.JTable usuariosTable;
    // End of variables declaration//GEN-END:variables
}
