package Vistas;

import Controladores.menuadmincontrolador;
import Controladores.menucolaboradorcontrolador;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class menuColaborador extends javax.swing.JFrame {
    menucolaboradorcontrolador mac;
    
    public menuColaborador() {
        initComponents();
        controlador();
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        this.ventasButton.setCursor(new Cursor(HAND_CURSOR));
        this.clientesButton.setCursor(new Cursor(HAND_CURSOR));
        this.cerrarsesionButton.setCursor(new Cursor(HAND_CURSOR));
        setTitle("      Menu-Colaborador");
        setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
        this.setLocationRelativeTo(this);
    }

    public void controlador(){
        mac = new menucolaboradorcontrolador(this);
        ventasButton.addActionListener(mac);
        clientesButton.addActionListener(mac);
        cerrarsesionButton.addActionListener(mac);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cerrarsesionButton = new javax.swing.JButton();
        clientesButton = new javax.swing.JButton();
        ventasButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        cerrarsesionButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btncerrarsesion.png"))); // NOI18N
        cerrarsesionButton.setActionCommand("cerrarsesion");
        cerrarsesionButton.setBorderPainted(false);
        cerrarsesionButton.setContentAreaFilled(false);
        cerrarsesionButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cerrarsesionButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                cerrarsesionButtonMouseExited(evt);
            }
        });
        getContentPane().add(cerrarsesionButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 240, 140, 80));

        clientesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnclientes.png"))); // NOI18N
        clientesButton.setActionCommand("clientes");
        clientesButton.setBorderPainted(false);
        clientesButton.setContentAreaFilled(false);
        clientesButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                clientesButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                clientesButtonMouseExited(evt);
            }
        });
        getContentPane().add(clientesButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 240, 140, 80));

        ventasButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnventas.png"))); // NOI18N
        ventasButton.setActionCommand("ventas");
        ventasButton.setBorderPainted(false);
        ventasButton.setContentAreaFilled(false);
        ventasButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                ventasButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ventasButtonMouseExited(evt);
            }
        });
        getContentPane().add(ventasButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 240, 140, 80));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/nuevos-productos.gif"))); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/productos.gif"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/fondo_menuAdmin.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ventasButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ventasButtonMouseEntered
         ventasButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnventasefecto.png")));
    }//GEN-LAST:event_ventasButtonMouseEntered

    private void ventasButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ventasButtonMouseExited
        ventasButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnventas.png")));
    }//GEN-LAST:event_ventasButtonMouseExited

    private void clientesButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_clientesButtonMouseEntered
        clientesButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnclientesefecto.png")));
    }//GEN-LAST:event_clientesButtonMouseEntered

    private void clientesButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_clientesButtonMouseExited
         clientesButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnclientes.png")));
    }//GEN-LAST:event_clientesButtonMouseExited

    private void cerrarsesionButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cerrarsesionButtonMouseEntered
        cerrarsesionButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btncerrarsesionefecto.png")));
    }//GEN-LAST:event_cerrarsesionButtonMouseEntered

    private void cerrarsesionButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cerrarsesionButtonMouseExited
        cerrarsesionButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btncerrarsesion.png")));
    }//GEN-LAST:event_cerrarsesionButtonMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(menuColaborador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(menuColaborador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(menuColaborador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(menuColaborador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new menuColaborador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cerrarsesionButton;
    private javax.swing.JButton clientesButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JButton ventasButton;
    // End of variables declaration//GEN-END:variables
}
