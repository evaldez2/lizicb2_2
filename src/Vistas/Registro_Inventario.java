package Vistas;

import Controladores.Registro_inventario_controlador;
import Controladores.Registro_usuarios_controlador;
import Modelos.ame_usuario_modelo;
import Modelos.cargartablas;
import Modelos.clsExportarExcel;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Registro_Inventario extends javax.swing.JFrame {

    cargartablas ut = new cargartablas();
    Registro_inventario_controlador ruc;

    public Registro_Inventario() {
        initComponents();
        controlador();
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        ut.cargartablainventario(usuariosTable);
        setTitle("      Registro de inventario");
        setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
        this.setLocationRelativeTo(this);
        this.refrescartablaButton.setCursor(new Cursor(HAND_CURSOR));
        this.regresarButton.setCursor(new Cursor(HAND_CURSOR));
        this.añadirButton.setCursor(new Cursor(HAND_CURSOR));
        this.modificarButton.setCursor(new Cursor(HAND_CURSOR));
        this.eliminarButton.setCursor(new Cursor(HAND_CURSOR));
    }

    public void controlador() {
        ruc = new Registro_inventario_controlador(this);
        añadirButton.addActionListener(ruc);
        modificarButton.addActionListener(ruc);
        eliminarButton.addActionListener(ruc);
        refrescartablaButton.addActionListener(ruc);
        regresarButton.addActionListener(ruc);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        usuariosTable = new javax.swing.JTable();
        añadirButton = new javax.swing.JButton();
        modificarButton = new javax.swing.JButton();
        eliminarButton = new javax.swing.JButton();
        refrescartablaButton = new javax.swing.JButton();
        regresarButton = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        usuariosTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Codigo", "Fecha", "Nombre", "Precio", "Cantidad_Total", "Cantidad_Vendida", "Cantidad_Disponible"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(usuariosTable);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 230, 800, 270));

        añadirButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadir.png"))); // NOI18N
        añadirButton.setActionCommand("Añadir");
        añadirButton.setBorderPainted(false);
        añadirButton.setContentAreaFilled(false);
        añadirButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                añadirButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                añadirButtonMouseExited(evt);
            }
        });
        getContentPane().add(añadirButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 150, 130, 70));

        modificarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1.png"))); // NOI18N
        modificarButton.setActionCommand("Modificar");
        modificarButton.setBorderPainted(false);
        modificarButton.setContentAreaFilled(false);
        modificarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                modificarButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                modificarButtonMouseExited(evt);
            }
        });
        getContentPane().add(modificarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 150, 130, 70));

        eliminarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btneliminar1.png"))); // NOI18N
        eliminarButton.setActionCommand("Eliminar");
        eliminarButton.setBorderPainted(false);
        eliminarButton.setContentAreaFilled(false);
        eliminarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                eliminarButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                eliminarButtonMouseExited(evt);
            }
        });
        getContentPane().add(eliminarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 150, 130, 70));

        refrescartablaButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnactualizar.png"))); // NOI18N
        refrescartablaButton.setActionCommand("RefrescarTabla");
        refrescartablaButton.setBorderPainted(false);
        refrescartablaButton.setContentAreaFilled(false);
        refrescartablaButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                refrescartablaButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                refrescartablaButtonMouseExited(evt);
            }
        });
        getContentPane().add(refrescartablaButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 150, 130, 70));

        regresarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnatras.png"))); // NOI18N
        regresarButton.setActionCommand("Regresar Al Menu");
        regresarButton.setBorderPainted(false);
        regresarButton.setContentAreaFilled(false);
        getContentPane().add(regresarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 10, 40, 30));

        jButton1.setText("EXPORTAR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 170, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/fondo_inventario.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void refrescartablaButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_refrescartablaButtonMouseEntered
        refrescartablaButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnactualizarefecto.png")));
    }//GEN-LAST:event_refrescartablaButtonMouseEntered

    private void refrescartablaButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_refrescartablaButtonMouseExited
       refrescartablaButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnactualizar.png")));
    }//GEN-LAST:event_refrescartablaButtonMouseExited

    private void añadirButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_añadirButtonMouseEntered
        añadirButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadirefecto.png")));
    }//GEN-LAST:event_añadirButtonMouseEntered

    private void añadirButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_añadirButtonMouseExited
        añadirButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadir.png")));
    }//GEN-LAST:event_añadirButtonMouseExited

    private void modificarButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modificarButtonMouseEntered
        modificarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1efecto.png")));
    }//GEN-LAST:event_modificarButtonMouseEntered

    private void modificarButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modificarButtonMouseExited
        modificarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1.png")));
    }//GEN-LAST:event_modificarButtonMouseExited

    private void eliminarButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_eliminarButtonMouseEntered
         eliminarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btneliminar1efecto.png")));
    }//GEN-LAST:event_eliminarButtonMouseEntered

    private void eliminarButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_eliminarButtonMouseExited
         eliminarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btneliminar1.png")));
    }//GEN-LAST:event_eliminarButtonMouseExited

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
          clsExportarExcel obj;
        try {
            obj = new clsExportarExcel();
            obj.exportarExcel(usuariosTable);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Registro_Inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Registro_Inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Registro_Inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Registro_Inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Registro_Inventario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton añadirButton;
    private javax.swing.JButton eliminarButton;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton modificarButton;
    private javax.swing.JButton refrescartablaButton;
    private javax.swing.JButton regresarButton;
    public javax.swing.JTable usuariosTable;
    // End of variables declaration//GEN-END:variables
}
