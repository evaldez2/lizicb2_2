package Vistas;

import Controladores.ame_ventas_controlador;
import Modelos.ame_ventas_modelo;
import Modelos.cargartablas;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class añadir_venta extends javax.swing.JFrame {

    cargartablas ct = new cargartablas();

    public añadir_venta() {
        initComponents();
        ct.cargartablaclientes(ClientesTable);
        ct.cargartablainventario2(InventarioTable);
        controlador();
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        setTitle("      Añadir Cliente");
        setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
        this.setLocationRelativeTo(this);
        this.añadirButton.setCursor(new Cursor(HAND_CURSOR));
        this.cerrarButton.setCursor(new Cursor(HAND_CURSOR));
    }

    public void controlador() {
        ame_ventas_controlador u = new ame_ventas_controlador(this);
        añadirButton.addActionListener(u);
        cerrarButton.addActionListener(u);
        RegistrarVentaButton.addActionListener(u);
    }

    public ame_ventas_modelo enviardatos() {
        ame_ventas_modelo m = new ame_ventas_modelo();
        m.setCodigocliente(idclienteTextField.getText());
        m.setNombrecliente(nombreclienteTextField.getText());
        m.setCodigoproducto(idproductoTextField.getText());
        m.setNombreproducto(nombreproductoTextField.getText());
        m.setCantidad(cantidadTextField.getText());
        m.setPrecio(precioTextField.getText());
        m.setTotal(totalTextField.getText());
        return m;
    }

    public void limpiar() {
        idclienteTextField.setText("");
        nombreclienteTextField.setText("");
        apellidoclienteTextField.setText("");
        idproductoTextField.setText("");
        nombreproductoTextField.setText("");
        cantidadTextField.setText("");
        precioTextField.setText("");
        totalTextField.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        añadirButton = new javax.swing.JButton();
        cerrarButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        precioTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        nombreproductoTextField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        idproductoTextField = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        totalTextField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        cantidadTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        apellidoclienteTextField = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        idclienteTextField = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        ClientesTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        InventarioTable = new javax.swing.JTable();
        buscarclienteTextField = new javax.swing.JTextField();
        buscarinventarioTextField = new javax.swing.JTextField();
        nombreclienteTextField = new javax.swing.JTextField();
        RegistrarVentaButton = new javax.swing.JButton();
        jLabel12 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        facturaTextField = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        TotalProductosTabla = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        añadirButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadir.png"))); // NOI18N
        añadirButton.setActionCommand("Añadir");
        añadirButton.setBorderPainted(false);
        añadirButton.setContentAreaFilled(false);
        añadirButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                añadirButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                añadirButtonMouseExited(evt);
            }
        });
        añadirButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                añadirButtonActionPerformed(evt);
            }
        });
        getContentPane().add(añadirButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 180, 130, 60));

        cerrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnexit.png"))); // NOI18N
        cerrarButton.setActionCommand("CAV");
        cerrarButton.setBorderPainted(false);
        cerrarButton.setContentAreaFilled(false);
        getContentPane().add(cerrarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 0, 20, 20));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Precio");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 120, -1, 20));

        precioTextField.setEditable(false);
        getContentPane().add(precioTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 120, 70, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Nombre");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 90, -1, 20));

        nombreproductoTextField.setEditable(false);
        getContentPane().add(nombreproductoTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 90, 170, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("ID_Prod");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 60, -1, -1));

        idproductoTextField.setEditable(false);
        getContentPane().add(idproductoTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 60, 60, -1));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Total");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 150, -1, 20));

        totalTextField.setEditable(false);
        getContentPane().add(totalTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 150, 80, -1));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Cantidad");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, -1, 20));

        cantidadTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                cantidadTextFieldKeyReleased(evt);
            }
        });
        getContentPane().add(cantidadTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 150, 80, -1));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Apellido");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, -1, 20));

        apellidoclienteTextField.setEditable(false);
        getContentPane().add(apellidoclienteTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 120, 170, -1));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("ID_Cliente");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, -1, -1));

        idclienteTextField.setEditable(false);
        getContentPane().add(idclienteTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 60, 70, -1));

        ClientesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "ID_Cliente", "Nombre", "Apellido"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        ClientesTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ClientesTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(ClientesTable);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 290, 250, 180));

        InventarioTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "ID_Prod", "Nombre", "Precio"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        InventarioTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                InventarioTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(InventarioTable);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 290, 260, 180));

        buscarclienteTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                buscarclienteTextFieldKeyReleased(evt);
            }
        });
        getContentPane().add(buscarclienteTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 260, 140, -1));

        buscarinventarioTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                buscarinventarioTextFieldKeyReleased(evt);
            }
        });
        getContentPane().add(buscarinventarioTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 260, 140, -1));

        nombreclienteTextField.setEditable(false);
        getContentPane().add(nombreclienteTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 90, 170, -1));

        RegistrarVentaButton.setText("Registrar Venta");
        RegistrarVentaButton.setActionCommand("RegistrarVenta");
        getContentPane().add(RegistrarVentaButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 200, -1, -1));

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Nombre");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, -1, 20));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Buscar Nombre Cliente");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 240, 140, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Buscar Nombre Producto");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 240, 160, -1));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Factura");
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, -1, 20));

        facturaTextField.setEditable(false);
        facturaTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                facturaTextFieldKeyReleased(evt);
            }
        });
        getContentPane().add(facturaTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 190, 80, -1));

        jButton1.setText("G");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 190, 50, 20));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ventana_añadir_ventas.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 510, 470));

        TotalProductosTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID Cliente", "Nombre", "ID Prod", "Nombre", "Precio", "Cantidad", "Total"
            }
        ));
        jScrollPane3.setViewportView(TotalProductosTabla);

        getContentPane().add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 1, 340, 470));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cantidadTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidadTextFieldKeyReleased
        try {
            int precio = Integer.parseInt(precioTextField.getText());
            int cantidad = Integer.parseInt(cantidadTextField.getText());
            totalTextField.setText(String.valueOf(cantidad * precio));
        } catch (Exception e) {
        }
    }//GEN-LAST:event_cantidadTextFieldKeyReleased

    private void buscarclienteTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_buscarclienteTextFieldKeyReleased
        String Cliente = buscarclienteTextField.getText();
        ct.BuscarClienteTabla(ClientesTable, Cliente);
    }//GEN-LAST:event_buscarclienteTextFieldKeyReleased

    private void ClientesTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ClientesTableMouseClicked
        int Selection = ClientesTable.rowAtPoint(evt.getPoint());
        idclienteTextField.setText(String.valueOf(ClientesTable.getValueAt(Selection, 0)));
        nombreclienteTextField.setText(String.valueOf(ClientesTable.getValueAt(Selection, 1)));
        apellidoclienteTextField.setText(String.valueOf(ClientesTable.getValueAt(Selection, 2)));

    }//GEN-LAST:event_ClientesTableMouseClicked

    private void buscarinventarioTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_buscarinventarioTextFieldKeyReleased
        String Inventario = buscarinventarioTextField.getText();
        ct.BuscarInventarioTabla(InventarioTable, Inventario);
    }//GEN-LAST:event_buscarinventarioTextFieldKeyReleased

    private void InventarioTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_InventarioTableMouseClicked
        int Selection = InventarioTable.rowAtPoint(evt.getPoint());
        idproductoTextField.setText(String.valueOf(InventarioTable.getValueAt(Selection, 0)));
        nombreproductoTextField.setText(String.valueOf(InventarioTable.getValueAt(Selection, 1)));
        precioTextField.setText(String.valueOf(InventarioTable.getValueAt(Selection, 2)));
    }//GEN-LAST:event_InventarioTableMouseClicked

    private void añadirButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_añadirButtonMouseEntered
        añadirButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadirefecto.png")));
    }//GEN-LAST:event_añadirButtonMouseEntered

    private void añadirButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_añadirButtonMouseExited
        añadirButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadir.png")));
    }//GEN-LAST:event_añadirButtonMouseExited

    private void facturaTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_facturaTextFieldKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_facturaTextFieldKeyReleased

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int numero = (int)(Math.random()*10000+1);
        facturaTextField.setText(String.valueOf(numero));
        this.jButton1.setEnabled(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void añadirButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_añadirButtonActionPerformed
        totalTextField.setText("");
        cantidadTextField.setText("");
    }//GEN-LAST:event_añadirButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(añadir_venta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(añadir_venta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(añadir_venta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(añadir_venta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new añadir_venta().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable ClientesTable;
    private javax.swing.JTable InventarioTable;
    private javax.swing.JButton RegistrarVentaButton;
    public javax.swing.JTable TotalProductosTabla;
    public javax.swing.JTextField apellidoclienteTextField;
    private javax.swing.JButton añadirButton;
    private javax.swing.JTextField buscarclienteTextField;
    private javax.swing.JTextField buscarinventarioTextField;
    public javax.swing.JTextField cantidadTextField;
    public javax.swing.JButton cerrarButton;
    public javax.swing.JTextField facturaTextField;
    private javax.swing.JTextField idclienteTextField;
    private javax.swing.JTextField idproductoTextField;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    public javax.swing.JTextField nombreclienteTextField;
    public javax.swing.JTextField nombreproductoTextField;
    public javax.swing.JTextField precioTextField;
    public javax.swing.JTextField totalTextField;
    // End of variables declaration//GEN-END:variables
}
