package Vistas;

import Controladores.Registro_ventas_colaborador_controlador;
import Controladores.Registro_ventas_controlador;
import Controladores.ame_ventas_controlador;
import Controladores.menuadmincontrolador;
import Modelos.cargartablas;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class Registro_Ventas_Colaborador extends javax.swing.JFrame {
    menuadmincontrolador mac;
    cargartablas ct = new cargartablas();
    
    public Registro_Ventas_Colaborador() {
        initComponents();
        setController();
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        ct.cargartablaventas(ventastotalesTable);
        setTitle("      Registro de Ventas");
        setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
        this.setLocationRelativeTo(this);
        this.refrescartablaButton.setCursor(new Cursor(HAND_CURSOR));
        this.regresarButton.setCursor(new Cursor(HAND_CURSOR));
        this.añadirButton.setCursor(new Cursor(HAND_CURSOR));
        this.modificarButton.setCursor(new Cursor(HAND_CURSOR));
    }
    
    public void setController(){
        Registro_ventas_colaborador_controlador a = new Registro_ventas_colaborador_controlador(this);
        añadirButton.addActionListener(a); 
        modificarButton.addActionListener(a);
        regresarButton.addActionListener(a);
        refrescartablaButton.addActionListener(a);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        ventastotalesTable = new javax.swing.JTable();
        refrescartablaButton = new javax.swing.JButton();
        modificarButton = new javax.swing.JButton();
        añadirButton = new javax.swing.JButton();
        regresarButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ventastotalesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "N° Fact", "Cod. Cli", "Fecha", "Nombre_Cli", "Cod. Prod", "Nombre Prod.", "Cantidad", "Precio U.", "Total", "Estado_Venta"
            }
        ));
        jScrollPane1.setViewportView(ventastotalesTable);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 140, 800, 360));

        refrescartablaButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnactualizar.png"))); // NOI18N
        refrescartablaButton.setActionCommand("RefrescarTabla");
        refrescartablaButton.setBorderPainted(false);
        refrescartablaButton.setContentAreaFilled(false);
        refrescartablaButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                refrescartablaButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                refrescartablaButtonMouseExited(evt);
            }
        });
        getContentPane().add(refrescartablaButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 60, 130, 70));

        modificarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1.png"))); // NOI18N
        modificarButton.setActionCommand("Modificar");
        modificarButton.setBorderPainted(false);
        modificarButton.setContentAreaFilled(false);
        modificarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                modificarButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                modificarButtonMouseExited(evt);
            }
        });
        getContentPane().add(modificarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 60, 120, 70));

        añadirButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadir.png"))); // NOI18N
        añadirButton.setActionCommand("Añadir");
        añadirButton.setBorderPainted(false);
        añadirButton.setContentAreaFilled(false);
        añadirButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                añadirButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                añadirButtonMouseExited(evt);
            }
        });
        getContentPane().add(añadirButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 60, 130, 70));

        regresarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnatras.png"))); // NOI18N
        regresarButton.setActionCommand("Regresar Al Menu");
        regresarButton.setBorderPainted(false);
        regresarButton.setContentAreaFilled(false);
        getContentPane().add(regresarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 10, 40, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/fondo_ventas.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void refrescartablaButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_refrescartablaButtonMouseEntered
         refrescartablaButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnactualizarefecto.png")));
    }//GEN-LAST:event_refrescartablaButtonMouseEntered

    private void refrescartablaButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_refrescartablaButtonMouseExited
         refrescartablaButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnactualizar.png")));
    }//GEN-LAST:event_refrescartablaButtonMouseExited

    private void añadirButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_añadirButtonMouseEntered
        añadirButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadirefecto.png")));
    }//GEN-LAST:event_añadirButtonMouseEntered

    private void añadirButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_añadirButtonMouseExited
        añadirButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadir.png")));
    }//GEN-LAST:event_añadirButtonMouseExited

    private void modificarButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modificarButtonMouseEntered
       modificarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1efecto.png")));
    }//GEN-LAST:event_modificarButtonMouseEntered

    private void modificarButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modificarButtonMouseExited
        modificarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1.png")));
    }//GEN-LAST:event_modificarButtonMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Registro_Ventas_Colaborador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Registro_Ventas_Colaborador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Registro_Ventas_Colaborador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Registro_Ventas_Colaborador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Registro_Ventas_Colaborador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton añadirButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton modificarButton;
    private javax.swing.JButton refrescartablaButton;
    private javax.swing.JButton regresarButton;
    public javax.swing.JTable ventastotalesTable;
    // End of variables declaration//GEN-END:variables
}
