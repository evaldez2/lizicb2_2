package Vistas;

import Controladores.Registro_ventas_controlador;
import Controladores.ame_ventas_controlador;
import Controladores.menuadmincontrolador;
import Modelos.ame_ventas_modelo;
import Modelos.cargartablas;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class consultar_ventas extends javax.swing.JFrame {

    menuadmincontrolador mac;
    cargartablas ct = new cargartablas();

    public consultar_ventas() {
        initComponents();
        //ct.cargartablaventas(ventastotalesTable);
        setController();
        setTitle("      Modificar Ventas");
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
        this.setLocationRelativeTo(this);
        this.modificarButton.setCursor(new Cursor(HAND_CURSOR));
        this.cerrarButton.setCursor(new Cursor(HAND_CURSOR));
    }

    public void setController() {
        ame_ventas_controlador a = new ame_ventas_controlador(this);
        modificarButton.addActionListener(a);
        cerrarButton.addActionListener(a);
    }

    public ame_ventas_modelo enviardatos() {
        ame_ventas_modelo a = new ame_ventas_modelo();

        a.setNumerofactura(numerofacturaTextField.getText());

        return a;
    }

    public void limpiar() {
        numerofacturaTextField.setText("");

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        ventastotalesTable = new javax.swing.JTable();
        cerrarButton = new javax.swing.JButton();
        modificarButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        numerofacturaTextField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        ventastotalesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID Venta", "Cod. Cli", "Num Venta", "Fecha", "Nombre_Cli", "ID Detalle", "Cod. Prod", "Nombre Prod.", "Cantidad", "Precio U.", "Total", "Estado_Venta"
            }
        ));
        ventastotalesTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ventastotalesTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(ventastotalesTable);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 160, 800, 340));

        cerrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnexit.png"))); // NOI18N
        cerrarButton.setActionCommand("CCV");
        cerrarButton.setBorderPainted(false);
        cerrarButton.setContentAreaFilled(false);
        getContentPane().add(cerrarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 0, 20, 20));

        modificarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1.png"))); // NOI18N
        modificarButton.setActionCommand("Consulta");
        modificarButton.setBorderPainted(false);
        modificarButton.setContentAreaFilled(false);
        modificarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                modificarButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                modificarButtonMouseExited(evt);
            }
        });
        getContentPane().add(modificarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 80, 130, 70));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("N° Fact");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, -1, 20));
        getContentPane().add(numerofacturaTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 70, 50, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ventana_modifica_ventas.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ventastotalesTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ventastotalesTableMouseClicked
        int Selection = ventastotalesTable.rowAtPoint(evt.getPoint());
        numerofacturaTextField.setText(String.valueOf(ventastotalesTable.getValueAt(Selection, 0)));
    }//GEN-LAST:event_ventastotalesTableMouseClicked

    private void modificarButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modificarButtonMouseEntered
        modificarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1efecto.png")));
    }//GEN-LAST:event_modificarButtonMouseEntered

    private void modificarButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modificarButtonMouseExited
        modificarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1.png")));
    }//GEN-LAST:event_modificarButtonMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(consultar_ventas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(consultar_ventas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(consultar_ventas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(consultar_ventas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new consultar_ventas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cerrarButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton modificarButton;
    private javax.swing.JTextField numerofacturaTextField;
    public javax.swing.JTable ventastotalesTable;
    // End of variables declaration//GEN-END:variables
}
