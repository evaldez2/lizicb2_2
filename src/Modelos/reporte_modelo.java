package Modelos;

public class reporte_modelo {
    String reporte;

    public reporte_modelo(String reporte) {
        this.reporte = reporte;
    }
    
    public reporte_modelo() {
        this.reporte = "";
    }

    public String getReporte() {
        return reporte;
    }

    public void setReporte(String reporte) {
        this.reporte = reporte;
    }
}
