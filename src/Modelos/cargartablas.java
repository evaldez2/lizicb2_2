/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import ConexionSql.ConexionSqlserver;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class cargartablas {

    ConexionSqlserver cc = new ConexionSqlserver();
    Connection con = cc.getConnection();
    
    public void cargartablaauditproducto (JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM DBO.AUDITA_PRODUCTOS ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(16));
                v.add(rs.getString(17));
                v.add(rs.getString(6));
                v.add(rs.getString(14));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }
    
    public void cargartablaventas (JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM VENTAS ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }
     public void cargartablaventasmodificar (JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM VENTAS ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(2));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }
     
    public void cargartablausuario(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM USUARIOSVIEW ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void BuscarClienteTabla(JTable tabla, String nombre) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "EXEC BUSCAR_USUARIO '" + nombre + "'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                model.addRow(v);

                tabla.setModel(model);
            }
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void cargartablaclientes(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM CLIENTESVIEW ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getInt(4));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void cargartablainventario(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM PRODUCTOS ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getInt(7));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void cargartablainventario2(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM PRODUCTOS ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void BuscarInventarioTabla(JTable tabla, String nombre) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "EXEC BUSCAR_INVENTARIO '" + nombre + "'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                model.addRow(v);

                tabla.setModel(model);
            }
        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }
}
