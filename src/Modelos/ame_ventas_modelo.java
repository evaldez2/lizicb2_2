package Modelos;

public class ame_ventas_modelo {
    String numerofactura;
    String fecha;
    String codigocliente;
    String nombrecliente;
    String codigoproducto;
    String nombreproducto;
    String cantidad;
    String precio;
    String total;
    String estadoventa;

    public ame_ventas_modelo(String numerofactura, String fecha, String codigocliente, String nombrecliente, String codigoproducto, String nombreproducto, String cantidad, String precio, String total, String estadoventa) {
        this.numerofactura = numerofactura;
        this.fecha = fecha;
        this.codigocliente = codigocliente;
        this.nombrecliente = nombrecliente;
        this.codigoproducto = codigoproducto;
        this.nombreproducto = nombreproducto;
        this.cantidad = cantidad;
        this.precio = precio;
        this.total = total;
        this.estadoventa = estadoventa;
    }
    
    public ame_ventas_modelo() {
        this.numerofactura = "";
        this.fecha = "";
        this.codigocliente = "";
        this.nombrecliente = "";
        this.codigoproducto = "";
        this.nombreproducto = "";
        this.cantidad = "";
        this.precio = "";
        this.total = "";
        this.estadoventa = "";
    }

    public String getNumerofactura() {
        return numerofactura;
    }

    public void setNumerofactura(String numerofactura) {
        this.numerofactura = numerofactura;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCodigocliente() {
        return codigocliente;
    }

    public void setCodigocliente(String codigocliente) {
        this.codigocliente = codigocliente;
    }

    public String getNombrecliente() {
        return nombrecliente;
    }

    public void setNombrecliente(String nombrecliente) {
        this.nombrecliente = nombrecliente;
    }

    public String getCodigoproducto() {
        return codigoproducto;
    }

    public void setCodigoproducto(String codigoproducto) {
        this.codigoproducto = codigoproducto;
    }

    public String getNombreproducto() {
        return nombreproducto;
    }

    public void setNombreproducto(String nombreproducto) {
        this.nombreproducto = nombreproducto;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getEstadoventa() {
        return estadoventa;
    }

    public void setEstadoventa(String estadoventa) {
        this.estadoventa = estadoventa;
    }
    
    
}
