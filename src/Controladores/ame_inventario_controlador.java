package Controladores;

import ConexionSql.ConexionSqlserver;
import Modelos.ame_inventario_modelo;
import Modelos.cargartablas;
import Vistas.Registro_Inventario;
import Vistas.añadir_inventario;
import Vistas.eliminar_inventario;
import Vistas.modificar_inventario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class ame_inventario_controlador implements ActionListener {

    ConexionSqlserver cc = new ConexionSqlserver();
    Connection con = cc.getConnection();
    private Registro_Inventario ri = new Registro_Inventario();
    private cargartablas ut = new cargartablas();
    private añadir_inventario ai;
    private modificar_inventario mi;
    private eliminar_inventario ei;
    private ame_inventario_modelo ami;

    public ame_inventario_controlador(añadir_inventario ai) {
        super();
        this.ai = ai;
        ami = new ame_inventario_modelo();
    }

    public ame_inventario_controlador(modificar_inventario mi) {
        super();
        this.mi = mi;
        ami = new ame_inventario_modelo();
    }

    public ame_inventario_controlador(eliminar_inventario ei) {
        super();
        this.ei = ei;
        ami = new ame_inventario_modelo();
    }

    public añadir_inventario getAi() {
        return ai;
    }

    public void setAi(añadir_inventario ai) {
        this.ai = ai;
    }

    public modificar_inventario getMi() {
        return mi;
    }

    public void setMi(modificar_inventario mi) {
        this.mi = mi;
    }

    public eliminar_inventario getEi() {
        return ei;
    }

    public void setEi(eliminar_inventario ei) {
        this.ei = ei;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Añadir":
                añadir();
                break;
            case "Modificar":
                modificar();
                break;

            case "Eliminar":
                eliminar();
                break;

            case "Buscar":
                setdate();
                break;
            case "BuscarE":
                setdateE();
                break;
            case "CMI":
                mi.dispose();
                break;
            case "CEI":
                ei.dispose();
                break;
            case "CAI":
                ai.dispose();
                break;
        }
    }

    public void añadir() {
        ami = ai.enviardatos();
        String nombre = ami.getNombre();
        String precio = ami.getPrecio();
        String cant_total = ami.getCant_total();

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC REGISTRAR_INVENTARIO '" + nombre + "','" + precio + "','" + cant_total + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validador = rs.getString("Codigo");

                if (validador.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Ingresado Correctamente");
                    ai.limpiar();
                    ut.cargartablaclientes(ri.usuariosTable);
                } else {
                    JOptionPane.showMessageDialog(null, "Ya existe el inventario");
                }

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void modificar() {
        ami = mi.enviardatos();
        String codigo = ami.getCodigo();
        String nombre = ami.getNombre();
        String precio = ami.getPrecio();
        String cant_total = ami.getCant_total();
   

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC ACTUALIZAR_INVENTARIO '" + codigo + "','" + nombre + "','" + precio + "','" + cant_total + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validador = rs.getString("Codigo");

                if (validador.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                    mi.limpiar();
                    ut.cargartablaclientes(ri.usuariosTable);
                } else {
                    JOptionPane.showMessageDialog(null, "Ya existe el producto en inventario");
                }

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void setdate() {
        ami = mi.enviardatos();
        String cod = ami.getCodigo();

        try {
            String sql = "SELECT * FROM PRODUCTOS WHERE CODIGO = '" + cod + "'";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                mi.codigoTextField.setText(rs.getString("CODIGO"));
                mi.nombreTextField.setText(rs.getString("NOMBRE"));
                mi.precioTextField.setText(rs.getString("PRECIO"));
                mi.cantidadtotalTextField.setText(rs.getString("CANTIDAD_TOTAL"));
            }
        } catch (Exception e) {
        }
    }

    public void setdateE() {
        ami = ei.enviardatos();
        String cod = ami.getCodigo();

        try {
            String sql = "SELECT * FROM PRODUCTOS WHERE CODIGO = '" + cod + "'";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                ei.codigoTextField.setText(rs.getString("CODIGO"));
                ei.nombreTextField.setText(rs.getString("NOMBRE"));
                ei.precioTextField.setText(rs.getString("PRECIO"));
                ei.cantidadtotalTextField.setText(rs.getString("CANTIDAD_TOTAL"));
                ei.cantidaddisponibleTextField.setText(rs.getString("CANTIDAD_DISPONIBLE"));
                ei.cantidadvendidaTextField.setText(rs.getString("CANTIDAD_VENDIDA"));
            }
        } catch (Exception e) {
        }
    }

    public void eliminar() {
        ami = ei.enviardatos();
        String id = ami.getCodigo();

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC ELIMINAR_INVENTARIO '" + id + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validar = rs.getString("CODIGO");

                if (validar.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
                    ei.limpiar();
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
