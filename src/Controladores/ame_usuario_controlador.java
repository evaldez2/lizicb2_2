package Controladores;

import ConexionSql.ConexionSqlserver;
import Modelos.ame_usuario_modelo;
import Modelos.cargartablas;
import Vistas.Registro_usuarios;
import Vistas.añadir_usuario;
import Vistas.eliminar_usuario;
import Vistas.modificar_usuario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class ame_usuario_controlador implements ActionListener {

    ConexionSqlserver cc = new ConexionSqlserver();
    Connection con = cc.getConnection();
    private Registro_usuarios ru = new Registro_usuarios();
    private cargartablas ut = new cargartablas();
    private añadir_usuario au;
    private modificar_usuario mu;
    private eliminar_usuario eu;
    private ame_usuario_modelo ame;

    public ame_usuario_controlador(añadir_usuario au) {
        super();
        this.au = au;
        ame = new ame_usuario_modelo();
    }

    public ame_usuario_controlador(modificar_usuario mu) {
        super();
        this.mu = mu;
        ame = new ame_usuario_modelo();
    }

    public ame_usuario_controlador(eliminar_usuario eu) {
        super();
        this.eu = eu;
        ame = new ame_usuario_modelo();
    }

    public ame_usuario_modelo getAme() {
        return ame;
    }

    public void setAme(ame_usuario_modelo ame) {
        this.ame = ame;
    }

    public modificar_usuario getMu() {
        return mu;
    }

    public void setMu(modificar_usuario mu) {
        this.mu = mu;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Añadir":
                añadir();
                break;
            case "Modificar":
                modificar();
                break;

            case "Eliminar":
                eliminar();
                break;

            case "Buscar":
                setdate();
                break;
            case "BuscarE":
                setdateE();
                break;
            case "CMU":
                mu.dispose();
                break;
            case "CEU":
                eu.dispose();
                break;
            case "CAU":
                au.dispose();
                break;
        }
    }

    public void añadir() {
        ame = au.enviardatos();
        String usuario = ame.getUsuario();
        String contraseña = ame.getContraseña();
        String rol = ame.getTipo_rol();
        String nombre = ame.getNombre();
        String apellido = ame.getApellido();

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC REGISTRAR_USUARIO '" + usuario + "','" + contraseña + "','" + nombre + "','" + apellido + "', '" + rol + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validador = rs.getString("Codigo");

                if (validador.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Ingresado Correctamente");
                    au.limpiar();
                    ut.cargartablausuario(ru.usuariosTable);
                } else {
                    JOptionPane.showMessageDialog(null, "Ya existe el usuario");
                }

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void modificar() {
        ame = mu.enviardatos();
        String id = ame.getId_usuario();
        String usuario = ame.getUsuario();
        String contraseña = ame.getContraseña();
        String rol = ame.getTipo_rol();
        String nombre = ame.getNombre();
        String apellido = ame.getApellido();

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC ACTUALIZAR_USUARIO '" + id + "','" + usuario + "','" + contraseña + "','" + nombre + "','" + apellido + "', '" + rol + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validador = rs.getString("Codigo");

                if (validador.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                    mu.limpiar();
                    ut.cargartablausuario(ru.usuariosTable);
                } else {
                    JOptionPane.showMessageDialog(null, "Ya existe el usuario");
                }

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void setdate() {
        ame = mu.enviardatos();
        String id = ame.getId_usuario();

        try {
            String sql = "SELECT * FROM USUARIOSVIEW WHERE ID_USUARIO = '" + id + "'";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                mu.usuarioTextField.setText(rs.getString("USUARIO"));
                mu.contraseñaTextField.setText(rs.getString("CONTRASEÑA"));
                mu.nombreTextField.setText(rs.getString("NOMBRE"));
                mu.apellidoTextField.setText(rs.getString("APELLIDO"));
                mu.rolComboBox.setSelectedItem(String.valueOf(rs.getString("TIPO_ROL")));
            }
        } catch (Exception e) {
        }
    }

    public void setdateE() {
        ame = eu.enviardatos();
        String id = ame.getId_usuario();

        try {
            String sql = "SELECT * FROM USUARIOSVIEW WHERE ID_USUARIO = '" + id + "'";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                eu.usuarioTextField.setText(rs.getString("USUARIO"));
                eu.contraseñaTextField.setText(rs.getString("CONTRASEÑA"));
                eu.nombreTextField.setText(rs.getString("NOMBRE"));
                eu.apellidoTextField.setText(rs.getString("APELLIDO"));
                eu.rolComboBox.setSelectedItem(String.valueOf(rs.getString("TIPO_ROL")));
            }
        } catch (Exception e) {
        }
    }

    public void eliminar() {
        ame = eu.enviardatos();
        String id = ame.getId_usuario();

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC ELIMINAR_USUARIO '" + id + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validar = rs.getString("CODIGO");

                if (validar.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
                    eu.limpiar();
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
