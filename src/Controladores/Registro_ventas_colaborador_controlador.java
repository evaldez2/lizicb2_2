package Controladores;

import Modelos.cargartablas;
import Vistas.Registro_Clientes;
import Vistas.Registro_Ventas;
import Vistas.Registro_Ventas_Colaborador;
import Vistas.Registro_usuarios;
import Vistas.añadir_cliente;
import Vistas.añadir_usuario;
import Vistas.añadir_venta;
import Vistas.eliminar_cliente;
import Vistas.eliminar_usuario;
import Vistas.menuAdmin;
import Vistas.menuColaborador;
import Vistas.modificar_cliente;
import Vistas.modificar_usuario;
import Vistas.modificar_ventas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Registro_ventas_colaborador_controlador implements ActionListener {
    private cargartablas ut = new cargartablas();
    private final Registro_Ventas_Colaborador rv;

    public Registro_ventas_colaborador_controlador(Registro_Ventas_Colaborador rv) {
        super();
        this.rv = rv;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Añadir":
                añadir();
                break;
            case "Modificar":
                modificar();
                break;
            case "Eliminar":
                eliminar();
                break;
            case "RefrescarTabla":
                ut.cargartablaventas(rv.ventastotalesTable);
                break;
            case "Regresar Al Menu":
                regresar();
                break;
        }
    }

    public void añadir() {
        añadir_venta av = new añadir_venta();
        av.setVisible(true);
    }

    public void modificar() {
        modificar_ventas mv = new modificar_ventas();
        mv.setVisible(true);
    }

    public void eliminar() {
        eliminar_cliente eu = new eliminar_cliente();
        eu.setVisible(true);
    }
    
    public void regresar(){
        rv.dispose();
        menuColaborador mc = new menuColaborador();
        mc.setVisible(true);
    }
}
