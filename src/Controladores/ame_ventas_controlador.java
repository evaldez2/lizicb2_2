package Controladores;

import ConexionSql.ConexionSqlserver;
import Modelos.ame_ventas_modelo;
import Modelos.cargartablas;
import Vistas.Registro_Ventas;
import Vistas.añadir_venta;
import Vistas.consultar_ventas;
import Vistas.modificar_ventas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class ame_ventas_controlador implements ActionListener {

    ConexionSqlserver cc = new ConexionSqlserver();
    Connection con = cc.getConnection();

    private Registro_Ventas rv = new Registro_Ventas();
    private cargartablas ut = new cargartablas();
    private añadir_venta av;
    private modificar_ventas mv;
    private ame_ventas_modelo ame;
    private consultar_ventas cv;
    
    public ame_ventas_controlador(consultar_ventas cv){
        super();
        this.cv =cv;
        ame = new ame_ventas_modelo();
    }

    public ame_ventas_controlador(añadir_venta av) {
        super();
        this.av = av;
        ame = new ame_ventas_modelo();
    }

    public ame_ventas_controlador(modificar_ventas mv) {
        super();
        this.mv = mv;
        ame = new ame_ventas_modelo();
    }

    public ame_ventas_modelo getAme() {
        return ame;
    }

    public void setAme(ame_ventas_modelo ame) {
        this.ame = ame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Añadir":
                añadir();
                break;

            case "Modificar":
    
                modificar();
                break;

            case "CMV":
                mv.dispose();
                break;
            case "CCV":
                cv.dispose();
                break;

            case "CAV":
                av.dispose();
                break;

            case "RegistrarVenta":
                registrarventa();
                break;
            case "Consulta":
                consulta();
                break;

        }
    }

    public void registrarventa() {
        if (av.TotalProductosTabla.getRowCount() > 0) {
                try {
                    String sql = "DECLARE @A INT;"
                            + "EXEC REGISTRAR_VENTAS '" + av.TotalProductosTabla.getValueAt(0, 0) + "','"+Integer.parseInt(av.facturaTextField.getText())+"','" + av.TotalProductosTabla.getValueAt(0, 1)+"',  @A OUTPUT;"
                            + "SELECT @A CODIGO;";

                    Statement st = con.createStatement();
                    ResultSet rs = st.executeQuery(sql);

                    if (rs.next()) {
                        String validador = rs.getString("Codigo");

                        if (validador.equals("0")) {
                            JOptionPane.showMessageDialog(null, "Ingresado Correctamente");
                            av.limpiar();
                            //ut.cargartablaventas(rv.ventastotalesTable);
                        } else if (validador.equals("1")) {
                            JOptionPane.showMessageDialog(null, "No hay producto en stock");
                        }

                    }
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        for (int i = 0; i < av.TotalProductosTabla.getRowCount(); i++) {
                try {
                    String sql1 = "DECLARE @A INT;"
                            + "EXEC REGISTRAR_detalle '"+Integer.parseInt(av.facturaTextField.getText())+"','"+av.TotalProductosTabla.getValueAt(i, 2)+"','"+av.TotalProductosTabla.getValueAt(i, 3)+"','"+av.TotalProductosTabla.getValueAt(i, 4)+"','"+av.TotalProductosTabla.getValueAt(i, 5)+"','"+av.TotalProductosTabla.getValueAt(i, 6)+"',@A OUTPUT;"
                            + "SELECT @A CODIGO;";

                    Statement st1 = con.createStatement();
                    ResultSet rs1 = st1.executeQuery(sql1);

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
    }

    /*public void añadir() {
        ame = av.enviardatos();
        String idcliente = ame.getCodigocliente();
        String nombrecliente = ame.getNombrecliente();
        String idproducto = ame.getCodigoproducto();
        String nombreproducto = ame.getNombreproducto();
        String cantidad = ame.getCantidad();
        String precio = ame.getPrecio();
        String total = ame.getTotal();

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC REGISTRAR_VENTAS '" + idcliente + "','" + nombrecliente + "','" + idproducto + "','" + nombreproducto + "', '" + cantidad + "', '" + precio + "', '" + total + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validador = rs.getString("Codigo");

                if (validador.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Ingresado Correctamente");
                    av.limpiar();
                    //ut.cargartablaventas(rv.ventastotalesTable);
                } else if (validador.equals("1")) {
                    JOptionPane.showMessageDialog(null, "No hay producto en stock");
                }

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }*/
    public void añadir() {
        ame = av.enviardatos();
        String idcliente = ame.getCodigocliente();
        String nombrecliente = ame.getNombrecliente();
        String idproducto = ame.getCodigoproducto();
        String nombreproducto = ame.getNombreproducto();
        String cantidad = ame.getCantidad();
        String precio = ame.getPrecio();
        String total = ame.getTotal();

        DefaultTableModel modelo = (DefaultTableModel) av.TotalProductosTabla.getModel();

        //Sección 2
        Object[] fila = new Object[7];

        //Sección 3
        fila[0] = idcliente;
        fila[1] = nombrecliente;
        fila[2] = idproducto;
        fila[3] = nombreproducto;
        fila[4] = cantidad;
        fila[5] = precio;
        fila[6] = total;

        //Sección 4
        modelo.addRow(fila);
    }
    
    public void consulta(){
        ame = cv.enviardatos();
        String numerofactura = ame.getNumerofactura();

                DefaultTableModel model = (DefaultTableModel) cv.ventastotalesTable.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        cv.ventastotalesTable.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "EXEC CONSULTAVENTA '"+numerofactura+"'";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                v.add(rs.getString(9));
                v.add(rs.getString(10));
                v.add(rs.getString(11));
                v.add(rs.getString(12));
                model.addRow(v);

                cv.ventastotalesTable.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void modificar() {
        ame = mv.enviardatos();
        String numerofactura = ame.getNumerofactura();
        String estado = ame.getEstadoventa();
        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC ACTUALIZAR_VENTA '" + numerofactura + "','" + estado + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
               /* String validador = rs.getString("Codigo");

                if (validador.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                    mv.limpiar();

                    //ut.cargartablausuario(rv.ventastotalesTable);
                } else if (validador.equals("1")) {
                    JOptionPane.showMessageDialog(null, "No se puede cambiar el estado, genere una nueva factura");
                }*/

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /*public void setdate() {
        ame = mv.enviardatos();
        String id = ame.getId_usuario();

        try {
            String sql = "SELECT * FROM USUARIOSVIEW WHERE ID_USUARIO = '" + id + "'";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                mv.usuarioTextField.setText(rs.getString("USUARIO"));
                mv.contraseñaTextField.setText(rs.getString("CONTRASEÑA"));
                mv.nombreTextField.setText(rs.getString("NOMBRE"));
                mv.apellidoTextField.setText(rs.getString("APELLIDO"));
                mv.rolComboBox.setSelectedItem(String.valueOf(rs.getString("TIPO_ROL")));
            }
        } catch (Exception e) {
        }
    }*/

 /*public void setdateE() {
        ame = ev.enviardatos();
        String id = ame.getId_usuario();

        try {
            String sql = "SELECT * FROM USUARIOSVIEW WHERE ID_USUARIO = '" + id + "'";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                ev.usuarioTextField.setText(rs.getString("USUARIO"));
                ev.contraseñaTextField.setText(rs.getString("CONTRASEÑA"));
                ev.nombreTextField.setText(rs.getString("NOMBRE"));
                ev.apellidoTextField.setText(rs.getString("APELLIDO"));
                ev.rolComboBox.setSelectedItem(String.valueOf(rs.getString("TIPO_ROL")));
            }
        } catch (Exception e) {
        }
    }*/

 /* public void eliminar() {
        ame = ev.enviardatos();
        String id = ame.getId_usuario();

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC ELIMINAR_USUARIO '" + id + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validar = rs.getString("CODIGO");

                if (validar.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }*/
}
