/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Vistas.menuAdmin;
import Vistas.Herramientas;
import Vistas.Nuevo_Backup;


public class Herramientas_Controlador implements ActionListener{
    
    private final Herramientas hr;

    public Herramientas_Controlador(Herramientas hr) {
        super();
        this.hr = hr;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Backup":
                nuevoback();
                break;
            //case "Modificar":
               // modificar();
               // break;
            //case "Eliminar":
             //   eliminar();
              //  break;
            //case "RefrescarTabla":
            //    ut.cargartablaclientes(rc.usuariosTable);
            //    break;
            case "Regresar Al Menu":
                regresar();
                break;
        }
    }
    
     public void nuevoback() {
        Nuevo_Backup nb = new Nuevo_Backup();
        nb.setVisible(true);
    }
     
     public void regresar(){
        hr.dispose();
        menuAdmin ma = new menuAdmin();
        ma.setVisible(true);
    }
    
}
