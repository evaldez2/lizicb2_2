package Controladores;

import ConexionSql.ConexionSqlserver;
import Modelos.reporte_modelo;
import Vistas.Reportes;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class reporte_controlador implements ActionListener {

    private final Reportes r;
    reporte_modelo rm;
    
    ConexionSqlserver cc = new ConexionSqlserver();
    Connection con = cc.getConnection();

    public reporte_controlador(Reportes r) {
        super();
        this.r = r;
        this.rm = new reporte_modelo();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "generar_reporte":
                gen_reporte();
                break;
                
            case "cerrar":
                r.dispose();
                break;
        }
    }

    public void gen_reporte() {
        rm = r.enviardatos();

        String tipo_reporte = rm.getReporte();

        if (tipo_reporte.equals("VENTAS")) {
            try {
                String rutareporte = System.getProperty("user.dir") + "/src/Reportes/Reporte_Ventas.jasper";
                String Logo = System.getProperty("user.dir") + "/src/Recursos/ventana.png";
                Map<String,Object> parameters = new HashMap<>();
                parameters.put("Logo",Logo);
                JasperReport jasperreport = (JasperReport) JRLoader.loadObjectFromFile(rutareporte);
                JasperPrint print = null;
                print = JasperFillManager.fillReport(jasperreport, parameters, con);
                JasperViewer view = new JasperViewer(print, false);
                view.setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
                view.setTitle("Reporte Ventas");
                view.setVisible(true);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else if (tipo_reporte.equals("CLIENTES")) {
            try {
                String rutareporte = System.getProperty("user.dir") + "/src/Reportes/Reporte_Clientes.jasper";
                String Logo = System.getProperty("user.dir") + "/src/Recursos/ventana.png";
                Map<String,Object> parameters = new HashMap<>();
                parameters.put("Logo",Logo);
                JasperReport jasperreport = (JasperReport) JRLoader.loadObjectFromFile(rutareporte);
                JasperPrint print = null;
                print = JasperFillManager.fillReport(jasperreport, parameters, con);
                JasperViewer view = new JasperViewer(print, false);
                view.setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
                view.setTitle("Reporte Clientes");
                view.setVisible(true);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else if (tipo_reporte.equals("PRODUCTOS")) {
            try {
                String rutareporte = System.getProperty("user.dir") + "/src/Reportes/Reporte_Inventario.jasper";
                String Logo = System.getProperty("user.dir") + "/src/Recursos/ventana.png";
                Map<String,Object> parameters = new HashMap<>();
                parameters.put("Logo",Logo);
                JasperReport jasperreport = (JasperReport) JRLoader.loadObjectFromFile(rutareporte);
                JasperPrint print = null;
                print = JasperFillManager.fillReport(jasperreport, parameters, con);
                JasperViewer view = new JasperViewer(print, false);
                view.setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
                view.setTitle("Reporte Productos");
                view.setVisible(true);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

}
