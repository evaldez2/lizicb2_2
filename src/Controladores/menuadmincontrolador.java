package Controladores;

import Vistas.Herramientas;
import Vistas.Registro_Clientes;
import Vistas.Inicio_sesion;
import Vistas.Registro_Inventario;
import Vistas.Registro_Ventas;
import Vistas.Registro_usuarios;
import Vistas.Reportes;
import Vistas.menuAdmin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//IMPLEMENTAMOS ACTIONLISTENER PARA PODER CONTROLAR LOS BOTONES DE LA VISTA POR SU ACTIONCOMMAND
public class menuadmincontrolador implements ActionListener {
    //INSTANCIAMOS LA VISTA
    private final menuAdmin ma;

    //CREAMOS UN CONSTRUCTOR QUE CONTENGA LA INSTANCIA DE LA VISTA
    public menuadmincontrolador(menuAdmin ma) {
        super();
        this.ma = ma;
    }

    //CREAMOS EL ACTIONPERFORMED PARA PODER CONTROLAR LAS ACCIONES DE LOS BOTONES DE LA VISTA
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
             //CREAMOS LOS CASOS DE LOS BOTONES DONDE LOS NOMBRAMOS POR EL ACTIONCOMMAND
            case "ventas": //MISMO NOMBRE QUE LE PUSIMOS EN EL ACTION COMMAND
                ventas(); //MANDAMOS A LLAMAR LA FUNCION QUE TENEMOS ABAJO CON EL MISMO NOMBRE
                break; //SI SE HA SELECCIONADO VENTAS SE DETENDRA SOLAMENTE EN VENTAS
                
            case "clientes": //MISMO NOMBRE QUE LE PUSIMOS EN EL ACTION COMMAND
                clientes(); //MANDAMOS A LLAMAR LA FUNCION QUE TENEMOS ABAJO CON EL MISMO NOMBRE
                break; //SI SE HA SELECCIONADO CLIENTES SE DETENDRA SOLAMENTE EN CLIENTES
                
            case "usuarios": //MISMO NOMBRE QUE LE PUSIMOS EN EL ACTION COMMAND
                usuarios(); //MANDAMOS A LLAMAR LA FUNCION QUE TENEMOS ABAJO CON EL MISMO NOMBRE
                break;//SI SE HA SELECCIONADO USUARIOS SE DETENDRA SOLAMENTE EN USUARIOS
                
            case "inventario": //MISMO NOMBRE QUE LE PUSIMOS EN EL ACTION COMMAND
                inventario(); //MANDAMOS A LLAMAR LA FUNCION QUE TENEMOS ABAJO CON EL MISMO NOMBRE
                break;//SI SE HA SELECCIONADO INVENTARIO SE DETENDRA SOLAMENTE EN INVENTARIO
            
            case "reportes":
                reportes();
                break;
            case "Herramientas":
                herramientas();
                break;
            case "cerrarsesion": //MISMO NOMBRE QUE LE PUSIMOS EN EL ACTION COMMAND
                cerrarsesion(); //MANDAMOS A LLAMAR LA FUNCION QUE TENEMOS ABAJO CON EL MISMO NOMBRE
                break; //SI SE HA SELECCIONADO CERRARSESION SE DETENDRA SOLAMENTE EN CERRARSESION
        }
    }
    
    //CREAMOS LAS FUNCIONES DONDE DARA ACCIONES A REALIZAR Y LUEGO LA MANDAMOS A LOS CASE DE ARRIBA
    public void ventas() {
        ma.dispose(); //CERRAMOS LA VENTANA DEL MENU ADMIN
        Registro_Ventas v = new Registro_Ventas(); //INSTANCIAMOS LA CLASE A USAR (JFRAME)
        v.setVisible(true); // HACEMOS QUE SEA VISIBLE LA CLASE (JFRAME)
    }

    public void herramientas(){
        Herramientas h = new Herramientas();
        h.setVisible(true);
    }
    public void clientes() {
        ma.dispose(); //CERRAMOS LA VENTANA DEL MENU ADMIN
        Registro_Clientes c = new Registro_Clientes(); //INSTANCIAMOS LA CLASE A USAR (JFRAME)
        c.setVisible(true); // HACEMOS QUE SEA VISIBLE LA CLASE (JFRAME)
    }

    public void usuarios() {
        ma.dispose(); //CERRAMOS LA VENTANA DEL MENU ADMIN
        Registro_usuarios u = new Registro_usuarios(); //INSTANCIAMOS LA CLASE A USAR (JFRAME)
        u.setVisible(true); // HACEMOS QUE SEA VISIBLE LA CLASE (JFRAME)
    }

    public void inventario() {
        ma.dispose(); //CERRAMOS LA VENTANA DEL MENU ADMIN
        Registro_Inventario i = new Registro_Inventario(); //INSTANCIAMOS LA CLASE A USAR (JFRAME)
        i.setVisible(true); // HACEMOS QUE SEA VISIBLE LA CLASE (JFRAME)
    }
    
    public void reportes(){
        Reportes r = new Reportes();
        r.setVisible(true);
    }

    public void cerrarsesion() {
        ma.dispose(); //CERRAMOS LA VENTANA DEL MENU ADMIN
        Inicio_sesion is = new Inicio_sesion(); //INSTANCIAMOS LA CLASE A USAR (JFRAME)
        is.setVisible(true); // HACEMOS QUE SEA VISIBLE LA CLASE (JFRAME)
    }
}
