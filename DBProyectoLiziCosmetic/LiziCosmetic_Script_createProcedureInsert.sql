--***********************************************PROCEDIMIENTO REGISTRAR USUARIO
DROP PROCEDURE IF EXISTS REGISTRAR_USUARIO
GO

CREATE PROCEDURE REGISTRAR_USUARIO
	@USUARIO VARCHAR (40),
	@CONTRASEŅA VARCHAR (40),
	@NOMBRE VARCHAR (40),
	@APELLIDO VARCHAR (40),
	@TIPO_ROL VARCHAR (40),
	@RETURN INT OUTPUT

	AS
	BEGIN TRY
		IF NOT EXISTS (SELECT*FROM USUARIO WHERE USUARIO = @USUARIO)
			BEGIN
				DECLARE @ID_Persona TINYINT
				INSERT INTO USUARIO(USUARIO,CONTRASEŅA,TIPO_ROL,ID_PERSONA)
				VALUES(@USUARIO,@CONTRASEŅA,@TIPO_ROL,@ID_Persona)

				INSERT INTO PERSONA(NOMBRE,Apellido)
				VALUES (@NOMBRE,@APELLIDO)

				SET @ID_Persona = (SELECT ID_Persona FROM dbo.PERSONA WHERE Nombre = @NOMBRE)

				UPDATE USUARIO
					SET ID_PERSONA = @ID_Persona
					WHERE USUARIO = @USUARIO

				SET @RETURN=0
			END
			ELSE
			BEGIN
				SET @RETURN=1
			END
	END TRY

	BEGIN CATCH
		IF @@ERROR>1
		BEGIN
			SET @RETURN= @@ERROR
		END
	END CATCH

	DECLARE @N INT
	EXEC REGISTRAR_USUARIO 'admin','1234','Eliza','Valdez','administrador',@N output
	select @n Codigo
	--SELECT * FROM USUARIO;
	--SELECT * FROM AUDITA_USUARIO
----------------------------------------------------------------------------
--***********************************************PROCEDIMIENTO REGISTRAR CLIENTE
DROP PROCEDURE IF EXISTS REGISTRAR_CLIENTE
GO

CREATE PROCEDURE REGISTRAR_CLIENTE
	@NOMBRE VARCHAR (40),
	@APELLIDO VARCHAR (40),
	@TELEFONO INT,
	@RETURN INT OUTPUT

	AS

	if not exists (select * from dbo.CLIENTE WHERE TELEFONO = @TELEFONO)
		Begin
				DECLARE @ID_PERSONA INT
				DECLARE @ID_cliente INT
				
				INSERT INTO CLIENTE(TELEFONO,ID_PERSONA)
				VALUES(@TELEFONO,@ID_PERSONA)

				INSERT INTO PERSONA(NOMBRE,Apellido)
				VALUES (@NOMBRE,@APELLIDO)

				SET @ID_Persona = (SELECT ID_Persona FROM dbo.PERSONA WHERE Nombre = @NOMBRE)
				SET @ID_Cliente = (SELECT ID_CLIENTE FROM dbo.CLIENTE WHERE TELEFONO = @TELEFONO)

				UPDATE CLIENTE
					SET ID_PERSONA = @ID_Persona
					WHERE ID_CLIENTE = @ID_cliente
				SET @RETURN=0
		End
	else
		BEGIN
			SET @RETURN=1
		END

GO

----------------------------------------------------------------------------
--***********************************************PROCEDIMIENTO REGISTRAR INVENTARIO
DROP PROCEDURE IF EXISTS REGISTRAR_INVENTARIO
GO

CREATE PROCEDURE REGISTRAR_INVENTARIO
	@NOMBRE VARCHAR (40),
	@PRECIO INT,
	@CANTIDAD_TOTAL INT,
	@RETURN INT OUTPUT

	AS
	BEGIN TRY

	IF NOT EXISTS (SELECT * FROM PRODUCTOS WHERE NOMBRE = @NOMBRE)
	BEGIN
				DECLARE @CANTIDAD_DISPONIBLE INT
				DECLARE @CANTIDAD_VENDIDA INT
				DECLARE @FECHA DATE
				SET @FECHA = GETDATE()
				SET @CANTIDAD_VENDIDA = 0
				SET @CANTIDAD_DISPONIBLE = @CANTIDAD_TOTAL-@CANTIDAD_VENDIDA
				INSERT INTO PRODUCTOS(FECHA,NOMBRE,PRECIO,CANTIDAD_TOTAL,CANTIDAD_VENDIDA,CANTIDAD_DISPONIBLE)
				VALUES(@FECHA,@NOMBRE,@PRECIO,@CANTIDAD_TOTAL,@CANTIDAD_VENDIDA,@CANTIDAD_DISPONIBLE)
				SET @RETURN=0
	END
	ELSE
	BEGIN 
				SET @RETURN=1
	END
				
	END TRY

	BEGIN CATCH
		IF @@ERROR>1
		BEGIN
			SET @RETURN= @@ERROR
		END
	END CATCH
----------------------------------------------------------------------------
--***********************************************PROCEDIMIENTO REGISTRAR VENTA
DROP PROCEDURE IF EXISTS REGISTRAR_VENTAS
GO

CREATE PROCEDURE REGISTRAR_VENTAS
	@ID_CLIENTE INT,
	@num_venta int,
	@nombrecliente VARCHAR(50),	
	@RETURN INT OUTPUT
AS	
	
	BEGIN
	DECLARE @ESTADOVENTA VARCHAR(500)
	DECLARE @FECHA DATE
	SET @FECHA=GETDATE()
	SET @ESTADOVENTA = 'REALIZADO'

	INSERT INTO VENTAS(ID_CLIENTE,Num_Venta,FECHA,NOMBRECLIENTE,ESTADOVENTA)
	VALUES(@ID_CLIENTE,@NUM_VENTA,@FECHA,@nombrecliente,@ESTADOVENTA)



	SET @RETURN = 0;
	END

GO
----------------------------------------------------------------------------
CREATE PROCEDURE Registrar_Detalle
@Num_Venta INT,
	@CODIGO INT,
	@nombreproducto VARCHAR(50),
	@cantidad INT,
	@precio INT,
	@total INT,
	@return int output
AS
INSERT INTO DETALLEVENTAS(ID_VENTA,CODIGO,nombreproducto,cantidad,precio,total)
	VALUES((SELECT ID_VENTA FROM VENTAS WHERE Num_Venta=@NUM_VENTA),@CODIGO,@NOMBREPRODUCTO,@CANTIDAD,@PRECIO,@TOTAL)


	UPDATE PRODUCTOS
		SET CANTIDAD_VENDIDA = CANTIDAD_VENDIDA + @cantidad
		WHERE CODIGO = @CODIGO

	UPDATE PRODUCTOS
		SET CANTIDAD_DISPONIBLE = CANTIDAD_TOTAL - CANTIDAD_VENDIDA
		WHERE CODIGO = @CODIGO

		
	SET @RETURN = 0;

SELECT * FROM dbo.ventas