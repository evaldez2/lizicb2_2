--Usamos la base de datos master 
--EN BACKUP ACTUALIZAR
USE master
go

--Creamos el login con el que se inicia sesion en sql y/o java
--EN BACKUP ACTUALIZAR
CREATE LOGIN Lizi WITH PASSWORD=N'1234'
go

--Usamos nuestra base de datos
--EN BACKUP ACTUALIZAR
USE LIZICOSMETIC
GO


--Creamos un usuario para entrar a la base de datos
--EN BACKUP ACTUALIZAR
DROP USER Lili
GO

CREATE USER Lili FOR LOGIN Lizi
GO

----------------------------------------------------------
--Creamos permisos para SELECT
CREATE ROLE db_select
go
--EN BACKUP ACTUALIZAR LAS SIGUIENTES DOS 
ALTER ROLE db_select ADD MEMBER Lili
GO

GRANT SELECT ON SCHEMA::dbo TO db_select
GO
----------------------------------------------------------
--CREAMOS EL ROL PARA INSERTAR
CREATE ROLE db_insert
go
--EN BACKUP ACTUALIZAR LAS SIGUIENTES DOS 
ALTER ROLE db_insert ADD MEMBER Lili
GO

GRANT INSERT ON SCHEMA::dbo TO db_insert
GO
----------------------------------------------------------
--CREAMOS EL ROL UPDATE
CREATE ROLE db_update
go
--EN BACKUP ACTUALIZAR LAS SIGUIENTES DOS 
ALTER ROLE db_update ADD MEMBER Lili
GO

GRANT UPDATE ON SCHEMA::dbo TO db_update
GO
----------------------------------------------------------
--CREAMOS EL ROL DELETE
CREATE ROLE db_delete
go
--EN BACKUP ACTUALIZAR LAS SIGUIENTES DOS 
ALTER ROLE db_delete ADD MEMBER Lili
GO

GRANT DELETE ON SCHEMA::dbo TO db_delete
GO
----------------------------------------------------------
--CREAMOS EL ROL PARA EJECUTAR PROCEDIMIENTOS ALMACENADOS
CREATE ROLE db_exec
GO
--EN BACKUP ACTUALIZAR LAS SIGUIENTES DOS 
ALTER ROLE db_exec ADD MEMBER Lili

GRANT EXECUTE ON SCHEMA::dbo TO db_exec
GO  
-----------------------------------------------------------

select * from PRODUCTOS