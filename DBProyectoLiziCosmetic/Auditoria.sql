USE LIZICOSMETIC
GO
--****************************************************************
--						TABLAS ESPEJO
--****************************************************************
DROP TABLE IF EXISTS AUDITA_CLIENTE
GO
CREATE TABLE AUDITA_CLIENTE(
	AUDITA_ID TINYINT NOT NULL IDENTITY(1,1),
	Evento CHAR(3) NOT NULL CHECK (Evento IN('INS','UPD','DEL')),
	ID_CLIENTE TINYINT,
	ID_PERSONA TINYINt,
	NuevoTELEFONO INT,
	AntiguoTELEFONO INT,
	FechaEvento DATETIME NOT NULL DEFAULT GETDATE(),
	UsuarioEvento sysname NOT NULL DEFAULT SUSER_SNAME(),
	PRIMARY KEY(AUDITA_ID)
)
GO

DROP TABLE IF EXISTS AUDITA_PRODUCTOS
GO
CREATE TABLE AUDITA_PRODUCTOS(
	AUDITA_ID TINYINT NOT NULL IDENTITY(1,1),
	Evento CHAR(3) NOT NULL CHECK (Evento IN('INS','UPD','DEL')),
	CODIGO INT,
	NuevaFECHA DATETIME DEFAULT GETDATE(),
	AntiguaFECHA DATETIME,
	NuevoNOMBRE VARCHAR (40),
	AntiguoNOMBRE VARCHAR (40),
	NuevoPRECIO INT,
	AntiguoPRECIO INT,
	NuevaCANTIDAD_TOTAL INT,
	AntiguaCANTIDAD_TOTAL INT,
	NuevaCANTIDAD_VENDIDA INT,
	AntiguaCANTIDAD_VENDIDA INT,
	NuevaCANTIDAD_DISPONIBLE INT,
	AntiguaCANTIDAD_DISPONIBLE INT,
	FechaEvento DATETIME NOT NULL DEFAULT GETDATE(),
	UsuarioEvento sysname NOT NULL DEFAULT SUSER_SNAME(),
	PRIMARY KEY(AUDITA_ID)
)
GO

DROP TABLE IF EXISTS AUDITA_VENTAS
GO
CREATE TABLE AUDITA_VENTAS(
	AUDITA_ID TINYINT NOT NULL IDENTITY(1,1),
	Evento CHAR(3) NOT NULL CHECK (Evento IN('INS','UPD','DEL')),
	ID_VENTA INT,
	ID_CLIENTE INT,
	NuevoNum_Venta INT,
	AntiguoNum_Venta INT,
	NuevaFECHA DATETIME DEFAULT GETDATE(),
	AntiguaFECHA DATETIME,
	Nuevonombrecliente VARCHAR(50),
	Antiguonombrecliente VARCHAR(50),
	Nuevoestadoventa VARCHAR(50),
	Antiguoestadoventa VARCHAR(50),
	FechaEvento DATETIME NOT NULL DEFAULT GETDATE(),
	UsuarioEvento sysname NOT NULL DEFAULT SUSER_SNAME(),
	PRIMARY KEY(AUDITA_ID)
)
GO


--****************************************************************
--						TRIGGER INSERT
--****************************************************************
DROP TRIGGER IF EXISTS trRegistraInsercionEnCLIENTE
GO
CREATE TRIGGER trRegistraInsercionEnCLIENTE
ON dbo.CLIENTE FOR INSERT
AS 
BEGIN
	INSERT INTO dbo.AUDITA_CLIENTE(Evento,ID_CLIENTE,ID_PERSONA,NuevoTELEFONO)
	SELECT 'INS', I.ID_CLIENTE,I.ID_PERSONA,I.TELEFONO FROM INSERTED I
END
GO
--****************************************************************
--****************************************************************
DROP TRIGGER IF EXISTS trRegistraInsercionEnPRODUCTOS
GO
CREATE TRIGGER trRegistraInsercionEnPRODUCTOS
ON dbo.PRODUCTOS FOR INSERT
AS 
BEGIN
	INSERT INTO dbo.AUDITA_PRODUCTOS(Evento,CODIGO,NuevoNOMBRE,NuevoPRECIO,NuevaCANTIDAD_TOTAL,NuevaCANTIDAD_VENDIDA,
								     NuevaCANTIDAD_DISPONIBLE)
	SELECT 'INS', I.CODIGO,I.NOMBRE, I.PRECIO, I.CANTIDAD_TOTAL, I.CANTIDAD_VENDIDA, I.CANTIDAD_DISPONIBLE 
	FROM INSERTED I
END
GO
--***************************************************************
--***************************************************************
DROP TRIGGER IF EXISTS trRegistraInsercionEnVENTAS
GO
CREATE TRIGGER trRegistraInsercionEnVENTAS
ON dbo.VENTAS FOR INSERT
AS 
BEGIN
	INSERT INTO dbo.AUDITA_VENTAS(Evento,ID_VENTA,ID_CLIENTE,NuevoNum_Venta,Nuevonombrecliente,Nuevoestadoventa)
	SELECT 'INS', I.ID_VENTA,I.ID_CLIENTE,I.Num_Venta,I.nombrecliente,I.estadoventa
	FROM INSERTED I
END
GO
--****************************************************************
--						TRIGGER DELETE
--****************************************************************
DROP TRIGGER IF EXISTS trRegistraEliminacionEnCLIENTE
GO
CREATE TRIGGER trRegistraEliminacionEnCLIENTE
ON dbo.CLIENTE FOR DELETE
AS BEGIN
		INSERT INTO dbo.AUDITA_CLIENTE(Evento,ID_CLIENTE,ID_PERSONA,AntiguoTELEFONO)
		SELECT 'DEL', D.ID_CLIENTE,D.ID_PERSONA,D.TELEFONO FROM DELETED D
	END
GO
----****************************************************************
----****************************************************************
DROP TRIGGER IF EXISTS trRegistraEliminacionEnVENTAS
GO
CREATE TRIGGER trRegistraEliminacionEnVENTAS
ON dbo.VENTAS FOR DELETE
AS BEGIN
		INSERT INTO dbo.AUDITA_VENTAS(Evento,ID_VENTA,ID_CLIENTE,AntiguoNum_Venta,AntiguaFECHA,Antiguonombrecliente,
									  Antiguoestadoventa)
		SELECT 'DEL', D.ID_VENTA,D.ID_CLIENTE,D.Num_Venta,D.FECHA,D.nombrecliente,D.estadoventa FROM DELETED D
	END
GO
--****************************************************************
--						TRIGGER UPDATE
--****************************************************************
DROP TRIGGER IF EXISTS trRegistraActualizacionEnCLIENTE
GO
CREATE TRIGGER trRegistraActualizacionEnCLIENTE
ON dbo.CLIENTE AFTER UPDATE
AS BEGIN
	INSERT INTO dbo.AUDITA_CLIENTE(Evento,ID_CLIENTE,ID_PERSONA,AntiguoTELEFONO,NuevoTELEFONO)
	SELECT 'UPD', I.ID_CLIENTE,I.ID_PERSONA,D.TELEFONO,I.TELEFONO
	FROM INSERTED I INNER JOIN DELETED D ON I.ID_PERSONA = D.ID_PERSONA
	END
GO
--****************************************************************
--****************************************************************
DROP TRIGGER IF EXISTS trRegistraActualizacionEnPRODUCTOS
GO
CREATE TRIGGER trRegistraActualizacionEnPRODUCTOS
ON dbo.PRODUCTOS AFTER UPDATE
AS BEGIN
	INSERT INTO dbo.AUDITA_PRODUCTOS(Evento,CODIGO,AntiguaFECHA,AntiguoNOMBRE,NuevoNOMBRE,AntiguoPRECIO,NuevoPRECIO,
									 AntiguaCANTIDAD_TOTAL,NuevaCANTIDAD_TOTAL,AntiguaCANTIDAD_VENDIDA,NuevaCANTIDAD_VENDIDA,
									 AntiguaCANTIDAD_DISPONIBLE,NuevaCANTIDAD_DISPONIBLE)
	SELECT 'UPD', I.CODIGO,D.FECHA,I.FECHA,D.NOMBRE,I.NOMBRE,D.PRECIO,D.PRECIO,I.PRECIO,D.CANTIDAD_TOTAL,I.CANTIDAD_TOTAL,
	D.CANTIDAD_VENDIDA,I.CANTIDAD_VENDIDA
	FROM INSERTED I INNER JOIN DELETED D ON I.CODIGO = D.CODIGO
	END
GO
--****************************************************************
--****************************************************************
DROP TRIGGER IF EXISTS trRegistraActualizacionEnVENTAS
GO
CREATE TRIGGER trRegistraActualizacionEnVENTAS
ON dbo.VENTAS AFTER UPDATE
AS BEGIN
	INSERT INTO dbo.AUDITA_VENTAS(Evento,ID_VENTA,ID_CLIENTE,AntiguoNum_Venta,NuevoNum_Venta,AntiguaFECHA,NuevaFECHA,Antiguonombrecliente,
									 Nuevonombrecliente,Antiguoestadoventa,Nuevoestadoventa)
	SELECT 'UPD', I.ID_VENTA,I.ID_CLIENTE,I.Num_Venta,D.Num_Venta,I.FECHA,D.FECHA,D.nombrecliente,I.nombrecliente,
	D.estadoventa,I.estadoventa
	FROM INSERTED I INNER JOIN DELETED D ON I.ID_VENTA = D.ID_CLIENTE
	END
GO
