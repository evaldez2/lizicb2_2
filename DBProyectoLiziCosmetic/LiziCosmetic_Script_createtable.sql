DROP TABLE IF EXISTS PERSONA
GO

CREATE TABLE PERSONA(
	ID_PERSONA TINYINT IDENTITY(1,1),
	Nombre VARCHAR(30),
	Apellido VARCHAR(30),
	CONSTRAINT PK_ID_PERSONA PRIMARY KEY (ID_PERSONA)
)
GO


--******************************************TABLA1USUARIO
DROP TABLE IF EXISTS USUARIO
GO

CREATE TABLE USUARIO (
	ID_USUARIO INT IDENTITY (1,1),
	USUARIO VARCHAR (40),
	CONTRASEŅA VARCHAR (40),
	TIPO_ROL VARCHAR (40),
	ID_PERSONA TINYINT,
	CONSTRAINT PK_ID_USUARIO PRIMARY KEY (ID_USUARIO),
	CONSTRAINT FK_ID_PERSONA_U FOREIGN KEY (ID_PERSONA) REFERENCES PERSONA(ID_PERSONA)
)
GO
--------------------------------------------------------------
--******************************************TABLA CLIENTE
DROP TABLE IF EXISTS CLIENTE
GO

CREATE TABLE CLIENTE (
	ID_CLIENTE INT IDENTITY (1,1),
	TELEFONO INT,
	ID_PERSONA TINYINT,
	CONSTRAINT PK_ID_CLIENTE PRIMARY KEY (ID_CLIENTE),
	CONSTRAINT FK_ID_PERSONA_C FOREIGN KEY (ID_PERSONA) REFERENCES PERSONA(ID_PERSONA)
)
GO
--------------------------------------------------------------
--******************************************TABLA INVENTARIO
DROP TABLE IF EXISTS PRODUCTOS
GO

CREATE TABLE PRODUCTOS (
	CODIGO INT IDENTITY (1,1),
	FECHA DATE,
	NOMBRE VARCHAR (40),
	PRECIO INT,
	CANTIDAD_TOTAL INT,
	CANTIDAD_VENDIDA INT,
	CANTIDAD_DISPONIBLE INT,
	CONSTRAINT PK_CODIGO PRIMARY KEY (CODIGO)
)
GO
--------------------------------------------------------------
--******************************************TABLA VENTAS
DROP TABLE IF EXISTS VENTAS
GO

CREATE TABLE VENTAS(
	ID_VENTA INT IDENTITY(1,1),
	ID_CLIENTE INT,
	Num_Venta INT,
	FECHA DATE,
	nombrecliente VARCHAR(50),
	estadoventa VARCHAR(50),
	PRIMARY KEY (ID_VENTA),
	FOREIGN KEY (ID_CLIENTE) REFERENCES CLIENTE(ID_CLIENTE)
)
GO
--------------------------------------------------------------
--------------------------------------------------------------
--******************************************TABLA DETALLE VENTAS
DROP TABLE IF EXISTS DETALLEVENTAS
GO

CREATE TABLE DETALLEVENTAS(
	ID_DETALLE INT IDENTITY(1,1) PRIMARY KEY,
	ID_VENTA INT,
	CODIGO INT,
	nombreproducto VARCHAR(50),
	cantidad INT,
	precio INT,
	total INT,
	FOREIGN KEY (ID_VENTA) REFERENCES VENTAS(ID_VENTA),
	FOREIGN KEY (CODIGO) REFERENCES PRODUCTOS(CODIGO)
)
GO
---------------------------------------------------------------