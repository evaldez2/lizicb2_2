--******************PROCEDIMIENTO DE SELECCIÓN USUARIO
DROP PROCEDURE IF EXISTS SELECT_USUARIO
GO

CREATE PROCEDURE SELECT_USUARIO
	@Usuario VARCHAR(50) = NULL,
	@Contraseña VARCHAR(50) = NULL,
	@Return INT OUTPUT
AS
		IF EXISTS(SELECT * FROM USUARIO WHERE(Usuario=@Usuario AND Contraseña = @Contraseña))
		BEGIN
			SET @Return  = 0
		END
		ELSE
		BEGIN
			SET @Return  = 1
		END
GO

--*********************Procedimiento para seleccionar el tipo de usuario
DROP PROCEDURE IF EXISTS Tipo_Usuario
GO

CREATE PROCEDURE Tipo_Usuario
@Usuario VARCHAR(40)
AS
BEGIN
	SELECT TIPO_ROL FROM USUARIO WHERE Usuario=@Usuario
END
----------------------------------------------------------------------
DROP PROCEDURE IF EXISTS BUSCAR_USUARIO
GO

CREATE PROCEDURE BUSCAR_USUARIO
	@Name VARCHAR(50) = NULL
AS
	SELECT C.ID_CLIENTE,P.Nombre,P.Apellido FROM dbo.PERSONA P
	INNER JOIN dbo.CLIENTE C ON C.ID_PERSONA = P.ID_PERSONA
	WHERE P.Nombre LIKE @Name+'%' OR @Name IS NULL
GO
----------------------------------------------------------------------
DROP PROCEDURE IF EXISTS BUSCAR_INVENTARIO
GO

CREATE PROCEDURE BUSCAR_INVENTARIO
	@Name VARCHAR(50) = NULL
AS
	SELECT * FROM dbo.PRODUCTOS
	WHERE Nombre LIKE @Name+'%' OR @Name IS NULL
GO
----------------------------------------------------------------------
DROP PROCEDURE IF EXISTS CONSULTAVENTA
GO

CREATE PROCEDURE CONSULTAVENTA
	@ID VARCHAR(50) = NULL
	AS
	SELECT V.ID_VENTA,v.ID_CLIENTE,V.Num_Venta,V.FECHA,V.nombrecliente,D.ID_DETALLE,D.CODIGO,D.nombreproducto,D.cantidad,D.precio,D.total,V.estadoventa FROM VENTAS V
LEFT JOIN DETALLEVENTAS D ON D.ID_VENTA = V.ID_VENTA
	WHERE V.Num_Venta = @ID 

	select * from ventas
