--se usa la bd master
use master
go

--creamos el device
EXEC sp_addumpdevice 'disk', 
'LiziCosmeticBDII',
'E:\SQL\LiziCosmeticBDII.bak';
GO
-------------------------------------------------------------------
--Creando el backup de LIZICOSMETIC que haga uso del dispositivo---
BACKUP DATABASE LIZICOSMETIC
-------Indicamos el nombre del dispositivo-------------------------
TO LiziCosmeticBDII
-------Parámetros para indicar que es un dispositvo múltiple-------
WITH FORMAT, INIT, NAME = N'LiziCosmeticBDII';
GO

-------------------------------------------------------------------------------
--Restaurar información del backup---------------------------------------------
--Restaurando el listado de archivos solo de mi dispositivo LiziCosmeticBDII---
RESTORE FILELISTONLY FROM LiziCosmeticBDII
GO
--Restaurando el encabezado del dispositivo LiziCosmeticBDII---
RESTORE HEADERONLY FROM LiziCosmeticBDII
GO
--

-------------------------------------------------------------------------------
--Rutina para ejecutar n backup------------------------------------------------
DECLARE @BackupName VARCHAR(100)
SET @BackupName = N'LiziCosmeticBDII ' + FORMAT(GETDATE(),'yyyyMMdd_hhmmss')

BACKUP DATABASE LIZICOSMETIC
TO LiziCosmeticBDII
WITH NOFORMAT, NOINIT, NAME = @BackupName,
SKIP, NOREWIND, NOUNLOAD, STATS = 10--muestra el progreso (estadística) de la ejecucion del backup cada 10%--
GO

--SELECT * FROM sys.backup_devices
--GO